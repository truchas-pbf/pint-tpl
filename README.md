The purpose of the pint-tpl project is to make it easy to build and deploy the dependencies necessary to take advantage of the parallel-in-time capabilities of TruchasPBF. These dependencies currently include Trilinos, ArborX, DTK, and nlohmann/json (a JSON library that's much more fully featured than YAJL).

BUILDING:

Building the third party libraries should be really easy, the only known dependency is Boost (>=1.67).  Depending on how you have built/installed Boost, you make need to pass a "BOOST_DIR" variable on the cmake line.

```
mkdir build
cd build
cmake -DBOOST_DIR=/my/boost/path/if/not/set/by/a/module ..
```
Unless you specified a CMAKE_INSTALL_PREFIX, everything will be in `build/install`

Note - Trilinos can take a while to build.  Using lots of cores is recommended (e.g. make -j 16, which takes 10-15 minutes)
