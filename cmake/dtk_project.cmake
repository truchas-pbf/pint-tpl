if(SEARCH_FOR_DTK)
  message(STATUS "Searching for a suitable arborx library ...")
  find_package(DataTransferKit)
endif()

if(DataTransferKit_FOUND)
  list(APPEND projects_found DTK)
else()
  list(APPEND projects_to_build DTK)
  externalproject_add(DTK
    DEPENDS ARBORX
    PREFIX DTK
    URL ${TARFILE_DIR}/dtk.tar.gz
    CMAKE_ARGS -D CMAKE_BUILD_TYPE:STRING=${CMAKE_BUILD_TYPE}
               -D CMAKE_INSTALL_PREFIX:PATH=${CMAKE_INSTALL_PREFIX}
               -D BUILD_SHARED_LIBS=${BUILD_SHARED_LIBS}
               -D DataTransferKit_ENABLE_DataTransferKit=ON 
               -D DataTransferKit_ENABLE_TESTS=ON 
               -D TPL_Trilinos_DIR=${CMAKE_INSTALL_PREFIX} 
               -D TPL_BoostOrg_INCLUDE_DIRS=${BOOST_DIR}/include 
               -D CMAKE_CXX_EXTENSIONS=OFF 
               -D DataTransferKit_CXX11_FLAGS:STRING=-std=c++14
               -D CMAKE_CXX_STANDARD=14 
               -D DataTransferKit_ENABLE_OpenMP=OFF 

    LOG_DOWNLOAD 1
    LOG_CONFIGURE 1
    LOG_BUILD 1
    LOG_INSTALL 1
  )
endif()
