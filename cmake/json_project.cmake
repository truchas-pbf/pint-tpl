if(SEARCH_FOR_JSON)
  message(STATUS "Searching for a suitable nlohmann_json library ...")
  find_package(nlohmann_json)
endif()

if(nlohmann_json_FOUND)
  list(APPEND projects_found "JSON")
else()
  list(APPEND projects_to_build "JSON")
  externalproject_add(JSON
    PREFIX JSON
    URL ${TARFILE_DIR}/json.tar.gz
    CMAKE_ARGS -D CMAKE_BUILD_TYPE:STRING=${CMAKE_BUILD_TYPE}
               -D CMAKE_INSTALL_PREFIX:PATH=${CMAKE_INSTALL_PREFIX}
               -D BUILD_SHARED_LIBS=${BUILD_SHARED_LIBS}
    LOG_DOWNLOAD 1
    LOG_CONFIGURE 1
    LOG_BUILD 1
    LOG_INSTALL 1
  )
endif()
